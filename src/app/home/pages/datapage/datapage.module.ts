import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LayoutModule} from '../../layouts/layout.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DatapageComponent } from './datapage.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSliderModule} from '@angular/material/slider';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import { LagerComponent } from './lager/lager.component';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatButtonModule} from '@angular/material/button';
import {OverlayModule} from '@angular/cdk/overlay';
import { MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {OrderByPipe} from '../../../pipes/order-by.pipe';
const routes: Routes = [
  {
    path: '',
    component: DatapageComponent,
    children: [
      {
            path: '',
            component: LagerComponent,
      }
    ]
  }
];
@NgModule({
  declarations: [ DatapageComponent, LagerComponent,    OrderByPipe],
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule, ReactiveFormsModule, MatDialogModule, MatSliderModule, MatChipsModule, MatIconModule, MatTableModule,
    MatSortModule, MatPaginatorModule, MatProgressSpinnerModule, MatInputModule, MatSnackBarModule, MatButtonModule, OverlayModule, MatCardModule, MatSidenavModule,
  ],
  bootstrap: [],

})
export class DatapageModule { }
