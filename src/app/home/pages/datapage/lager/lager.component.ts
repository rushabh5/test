import {ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {LagerService} from '../../../../services/lager.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {catchError, map, take} from 'rxjs/operators';
import {interval, Observable, Subscription, throwError} from 'rxjs';
import {FormModel} from './model/FormModel';
import {DOCUMENT} from '@angular/common';
import {FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';


@Component({
  selector: 'app-lager',
  templateUrl: './lager.component.html',
  styleUrls: ['./lager.component.css']
})
export class LagerComponent implements OnInit {
  public employeeList: FormModel[];
  id: number;
  opened: boolean;
  gitlab = 'https://gitlab.com/rushabh5/test.git';
  interval: any;
  registrationForm: FormGroup;
  @ViewChild('fileInput') el: ElementRef;
  imageUrl: any = 'https://st.depositphotos.com/2101611/3925/v/600/depositphotos_39258143-stock-illustration-businessman-avatar-profile-picture.jpg';
  editFile: boolean = true;
  removeUpload: boolean = false;
  selectedUser = []


  constructor(private snackBar: MatSnackBar,
              private lagerService: LagerService,
              @Inject(DOCUMENT) private document: Document,
              public fb: FormBuilder,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      description: [''],
      Image: [null, Validators.compose([Validators.required])],
      groupUsers: [null, Validators.compose([Validators.required])],
    });

    this.loadProductsPage();
  }

  loadProductsPage() {
    this.lagerService.getLager() .pipe(
      catchError(err => {
        this.snackBar.open('Something went wrong', '', {duration: 4000,});
        return throwError(err);
      })
    ).subscribe(res => {
        this.employeeList = res;
        this.snackBar.open('Data Loaded Successfully', '', {duration: 2000,});
  });
  }

  goToUrl(): void {
    this.document.location.href = this.gitlab;
  }
  remove() {
    this.selectedUser =  [];
  }

  uploadFile(event) {
    const reader = new FileReader();
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.registrationForm.patchValue({
          Image: reader.result
        });
        this.editFile = false;
        this.removeUpload = true;
      };
      this.cd.markForCheck();
    }
  }

  onSubmit() {
    if ( !this.registrationForm.valid) { return ; }
    if (this.registrationForm.valid) {
      const custData = {
        name: this.controls.name.value,
        description: this.controls.description.value,
        groupUsers: this.controls.groupUsers.value,
        Image: this.controls.Image.value,
      };
      this.lagerService.saveEmp(custData).subscribe((res: any) => {
          this.snackBar.open('Data Saved Successfully', '', {duration: 2000,});
        },
        (error) => {
          this.snackBar.open('Something went wrong', '', {duration: 2000,});
        });
    }
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.registrationForm.controls[controlName];
    if (!control) return false;
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  get controls() {
    return this.registrationForm.controls;
  }

  selectUser(id) {
    if (this.selectedUser.includes(id)) {this.selectedUser = this.selectedUser.filter(x => x !== id); } else { this.selectedUser.push(id); }
    this.registrationForm.patchValue({
      groupUsers: this.selectedUser
    });
  }

}

