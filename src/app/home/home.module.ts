import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';
import {LayoutModule} from './layouts/layout.module';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: '.\/pages\/datapage\/datapage.module#DatapageModule'
      }
    ]
  }
];
@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, LayoutModule,
    ReactiveFormsModule,
  ],
  providers: [
  ]
})

export class HomeModule { }
