import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LagerService {
  constructor(private http: HttpClient) { }

  getLager(): Observable<any> {
    return this.http.get( 'https://s3-ap-southeast-1.amazonaws.com/he-public-data/users49b8675.json');
  }

  saveEmp(data): Observable<any> {
    return this.http.post('https://s3-ap-southeast-1.amazonaws.com/he-public-data/users49b8675.json', data);
  }
}

